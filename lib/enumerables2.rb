require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) do |sum, num|
    sum + num
  end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |long_string| substring?(long_string, substring) }
end

def substring?(long_string, substring)
  return true if long_string.include?(substring)
  false
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  non_uniques = string.chars.select { |letter| string.count(letter) > 1 }
  non_uniques.delete(" ")
  non_uniques.uniq!
  non_uniques.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  punctuation = ";.,!@'/?:'"
  sorted = string.split.sort_by { |word| word.delete(punctuation).length }
  sorted[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = ("a".."z").to_a
  string.each_char { |letter| letters.delete(letter) }
  letters
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = (first_yr..last_yr).to_a
  years.select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  non_repeats = songs.select { |song| no_repeats?(song, songs) }
  non_repeats.uniq
end

def no_repeats?(song_name, songs)
  songs.reduce do |previous_song, current_song|
    if previous_song == song_name && current_song == song_name
      return false
    else
      current_song
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  remove_punctuation(string)
  c_words = string.split.select { |word| word.downcase.include?("c") }
  return "" if c_words.empty?

  c_words.sort_by { |word| c_distance(word) }.first
end

def c_distance(word)
  word.chars.reverse.reduce(0) do |distance, letter|
    if letter == 'c'
      return distance
    else
      distance + 1
    end
  end
end

def remove_punctuation(word)
  punctuation = ";.,!@'/?:'"
  word.delete!(punctuation)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)

  repeating = false
  start_index = nil
  results_arr = []

  arr.each_with_index do |num, index|
    next_num = arr[index + 1]
    if next_num == num && !repeating
      start_index = index
      repeating = true
    elsif next_num != num && repeating
      results_arr << [start_index, index]
      repeating = false
    end
  end
  results_arr
end
